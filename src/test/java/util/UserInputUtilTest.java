package util;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static constants.Constants.*;
import static org.junit.Assert.assertEquals;

@RunWith(JUnit4.class)
public class UserInputUtilTest {

    private String inputString;
    private final String serverId = "serverName";
    private List<String> expectedOutputList;
    private Map<String, Boolean> alertResultsMap;

    @Before
    public void setUp() {
        String value1 = "10";
        String value2 = "10";
        String value3 = "10";

        inputString = "( " + serverId + DELIMITER + value1 + DELIMITER + value2 + DELIMITER + value3 + " )";

        expectedOutputList = new ArrayList<String>();
        expectedOutputList.add(serverId);
        expectedOutputList.add(value1);
        expectedOutputList.add(value2);
        expectedOutputList.add(value3);

        alertResultsMap = new HashMap<String, Boolean>();
        alertResultsMap.put(CPU_UTILIZATION, false);
        alertResultsMap.put(MEMORY_UTILIZATION, false);
        alertResultsMap.put(DISK_UTILIZATION, false);
    }

    @Test
    public void parseInputHappyPath() {
        List<String> result = UserInputUtil.parseInput(inputString);

        assertEquals(expectedOutputList,result);
    }

    @Test
    public void constructAlertMessage() {
        alertResultsMap.put(CPU_UTILIZATION, true);

        StringBuilder expectedOutput = new StringBuilder();
        expectedOutput.append("(")
                .append(ALERT)
                .append(DELIMITER)
                .append(serverId)
                .append(DELIMITER)
                .append(CPU_UTILIZATION)
                .append(")");

        String result = UserInputUtil.constructAlertMessage(serverId, alertResultsMap);

        assertEquals(expectedOutput.toString(),result);
    }

    @Test
    public void getUserInput(){
        InputStream inputStream = new ByteArrayInputStream(inputString.getBytes());
        System.setIn(inputStream);

        List<String> result = UserInputUtil.getUserInput();

        assertEquals(expectedOutputList, result);
    }

    @Test
    public void getResultsNoAlerts(){
        String expectedOutput = "(" + NO_ALERT + DELIMITER + serverId + ")";

        String result = UserInputUtil.getResults(serverId, alertResultsMap);

        assertEquals(expectedOutput, result);
    }

    @Test
    public void getResultsSingleAlert(){
        alertResultsMap.put(DISK_UTILIZATION, true);

        String expectedOutput = "(" + ALERT + DELIMITER + serverId + DELIMITER + DISK_UTILIZATION + ")";

        String result = UserInputUtil.getResults(serverId, alertResultsMap);

        assertEquals(expectedOutput, result);
    }

    @Test
    public void getResultsAlerts(){
        alertResultsMap.put(CPU_UTILIZATION, true);
        alertResultsMap.put(DISK_UTILIZATION, true);

        StringBuilder expectedOutput = new StringBuilder()
                                                        .append("(")
                                                        .append(ALERT)
                                                        .append(DELIMITER)
                                                        .append(serverId)
                                                        .append(DELIMITER)
                                                        .append(DISK_UTILIZATION)
                                                        .append(DELIMITER)
                                                        .append(CPU_UTILIZATION)
                                                        .append(")");

        String result = UserInputUtil.getResults(serverId, alertResultsMap);

        assertEquals(expectedOutput.toString(), result);
    }
}
