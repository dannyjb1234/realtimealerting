package util;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static constants.Constants.*;
import static junit.framework.TestCase.assertEquals;

@RunWith(JUnit4.class)
public class AlertUtilTest {

    private List<String> inputList;
    private Map<String, Boolean> expectedMap;

    @Before
    public void setUp() {
        inputList = new ArrayList<String>();
        inputList.add("serverName");
        inputList.add("0");
        inputList.add("0");
        inputList.add("0");

        expectedMap = new HashMap<String, Boolean>();
        expectedMap.put(CPU_UTILIZATION, false);
        expectedMap.put(MEMORY_UTILIZATION, false);
        expectedMap.put(DISK_UTILIZATION, false);
    }

    @Test
    public void getAlertStatusAllFalse() {
        Map<String, Boolean> result = AlertUtil.getAlertStatus(inputList);

        assertEquals(expectedMap, result);
    }

    @Test
    public void getAlertStatusOneTrue() {
        inputList.set(1, "100");
        expectedMap.put(CPU_UTILIZATION, true);

        Map<String, Boolean> result = AlertUtil.getAlertStatus(inputList);

        assertEquals(expectedMap, result);
    }

    @Test
    public void getAlertStatusAllTrue() {
        inputList.set(1, "100");
        inputList.set(2, "100");
        inputList.set(3, "100");
        expectedMap.put(CPU_UTILIZATION, true);
        expectedMap.put(MEMORY_UTILIZATION, true);
        expectedMap.put(DISK_UTILIZATION, true);

        Map<String, Boolean> result = AlertUtil.getAlertStatus(inputList);

        assertEquals(expectedMap, result);
    }

}
