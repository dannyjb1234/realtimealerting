package util;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static constants.Constants.CPU_UTILIZATION;
import static constants.Constants.MEMORY_UTILIZATION;
import static org.junit.Assert.assertTrue;

@RunWith(JUnit4.class)
public class ResourceLimitUtilTest {


    @Test
    public void isRuleViolatedTrue() {
        boolean result = ResourceLimitUtil.isRuleViolated(CPU_UTILIZATION, "100");
        assertTrue(result);
    }

    @Test
    public void isRuleViolatedFalse() {
        boolean result = ResourceLimitUtil.isRuleViolated(MEMORY_UTILIZATION, "10");
        Assert.assertFalse(result);
    }

}

