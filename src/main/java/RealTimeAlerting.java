import util.AlertUtil;
import util.UserInputUtil;

import java.util.List;
import java.util.Map;

public class RealTimeAlerting {

    public static void main(String[] args) {

        List<String> userInputList = UserInputUtil.getUserInput();

        String serverId = userInputList.get(0);

        Map<String, Boolean> alertStatusMap = AlertUtil.getAlertStatus(userInputList);

        System.out.println(UserInputUtil.getResults(serverId, alertStatusMap));

    }


}
