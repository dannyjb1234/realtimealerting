package constants;

public interface Constants {

    String CPU_UTILIZATION = "CPU_UTILIZATION";
    String MEMORY_UTILIZATION = "MEMORY_UTILIZATION";
    String DISK_UTILIZATION = "DISK_UTILIZATION";

    String ALERT = "Alert";
    String NO_ALERT = "No Alert";

    String DELIMITER = ",";

}
