package util;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static constants.Constants.*;
import static util.ResourceLimitUtil.isRuleViolated;

public class AlertUtil {

    private AlertUtil() {
    }

    public static Map<String, Boolean> getAlertStatus(List<String> metricInputList){
        Map<String, Boolean> alertMap = new HashMap<String, Boolean>();

        alertMap.put(CPU_UTILIZATION, isRuleViolated(CPU_UTILIZATION, metricInputList.get(1)));
        alertMap.put(MEMORY_UTILIZATION, isRuleViolated(MEMORY_UTILIZATION, metricInputList.get(2)));
        alertMap.put(DISK_UTILIZATION, isRuleViolated(DISK_UTILIZATION, metricInputList.get(3)));

        return alertMap;
    }

}
