package util;

import java.util.HashMap;
import java.util.Map;

import static constants.Constants.CPU_UTILIZATION;
import static constants.Constants.DISK_UTILIZATION;
import static constants.Constants.MEMORY_UTILIZATION;

class ResourceLimitUtil {

    private final static Map<String, Integer> resourceMap;

    private ResourceLimitUtil(){}

    static{
        resourceMap = new HashMap<String, Integer>();

        resourceMap.put(CPU_UTILIZATION, 85);
        resourceMap.put(MEMORY_UTILIZATION, 75);
        resourceMap.put(DISK_UTILIZATION , 60);
    }

    static boolean isRuleViolated(String resource, String usage){
        return resourceMap.get(resource) < Integer.valueOf(usage);
    }

}
