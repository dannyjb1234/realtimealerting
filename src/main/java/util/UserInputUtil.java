package util;

import java.util.*;

import static constants.Constants.ALERT;
import static constants.Constants.DELIMITER;
import static constants.Constants.NO_ALERT;

public class UserInputUtil {

    private UserInputUtil(){
    }

    public static List<String> getUserInput(){
        System.out.println("Please input values in the following format:");
        System.out.println("(SERVER_ID, CPU_UTILIZATION, MEMORY_UTILIZATION, DISK_UTILIZATION)");

        final Scanner scanner = new Scanner(System.in);

        String rawUserInput = scanner.nextLine();

        scanner.close();

        return parseInput(rawUserInput);
    }

    public static String getResults(String serverId, Map<String, Boolean> alertResults){
        String result;

        if(alertResults.containsValue(true)){
            result = constructAlertMessage(serverId, alertResults);
        }
        else{
            result = "(" + NO_ALERT + DELIMITER + serverId + ")";
        }

        return result;
    }

    static List<String> parseInput(String rawUserInput){

        String parsedInput = rawUserInput.replaceAll(" ", "")
                                         .replace("(", "")
                                         .replace(")", "");

        return Arrays.asList(parsedInput.split(DELIMITER));

    }

    static String constructAlertMessage(String serverId, Map<String, Boolean> alertResults){
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append("(")
                .append(ALERT)
                .append(DELIMITER)
                .append(serverId);

        for (String key: alertResults.keySet()) {
            if(alertResults.get(key)){
                stringBuilder.append(DELIMITER)
                        .append(key);
            }
        }

        return stringBuilder.append(")").toString();

    }

}
